/* Read a given from the user N numbers and return the MAXIMUM number */

#include <stdio.h>
#include <stdlib.h>

void getLine(char* line, const int line_size);

int i_readNumber();
float f_readNumber();
double d_readNumber();

int i_max(int num1, int num2);
float f_max(float num1, float num2);
double d_max(double num1, double num2);

int i_max_in_arr(const int* arr, const int arr_size);
float f_max_in_arr(const float* arr, const int arr_size);
double d_max_in_arr(const double* arr, const int arr_size);

void exec_program();

int main() {
    exec_program();
    return 0;
}

void exec_program(){
    /* Ask the user for the size of the array. */
    printf("Give the size of the list: ");
    const int arr_size = i_readNumber();
    printf("Number size: %d\n", arr_size);

    /* Allocate memory */
    float* num_arr;
    num_arr = (float*)malloc(arr_size*sizeof(float));

    /* Ask the user to specify the numbers. */
    for(int i=0; i<arr_size; i++){
        printf("Give number #%d: ", i+1);
        num_arr[i] = f_readNumber();
    }

    /* Show him the numbers he selected. */
    for(int i=0; i<arr_size-1; i++)
        printf("%f, ", num_arr[i]);
    printf("%f\n", num_arr[arr_size-1]);

    /* Find the maximum number and print it to screen. */
    float maxNum = f_max_in_arr(num_arr, arr_size);
    printf("Maximum: %f\n", maxNum);
    free(num_arr);
}

void getLine(char* line, const int line_size) {
    int index = 0;
    char c;
    while(index < line_size && (c=getchar())!=EOF && ((c >='0' && c <='9') || c == '.' || (c=='-' && index==0) || (c=='+' && index==0))){
        line[index++] = c;
    }
    if (index >= line_size){
        while((c=getchar())!='\n') ;
    }
}


int i_readNumber() {
    const int line_size = 9;
    char* line = (char *)malloc(line_size * sizeof(char));
    getLine(line, line_size);
    int number = atoi(line);
    free(line);
    return number;
}

float f_readNumber() {
    const int line_size = 9;
    char* line = malloc(line_size*sizeof(char));
    getLine(line, line_size);
    float number = atof(line);
    free(line);
    return number;
}

double d_readNumber() {
    const int line_size = 9;
    char* line = malloc(line_size*sizeof(char));
    getLine(line, line_size);
    double number = atof(line);
    free(line);
    return number;
}

int i_max(int num1, int num2) {
    return num1 > num2 ? num1 : num2;
}

float f_max(float num1, float num2) {
    return num1 > num2 ? num1 : num2;
}

double d_max(double num1, double num2) {
    return num1 > num2 ? num1 : num2;
}

int i_max_in_arr(const int* arr, const int arr_size) {
    int maxNum = arr[0];
    for (int i=0; i<arr_size; i++){
        maxNum = i_max(maxNum, arr[i]);
    }
    return maxNum;
}

float f_max_in_arr(const float* arr, const int arr_size) {
    float maxNum = arr[0];
    for (int i=0; i<arr_size; i++){
        maxNum = f_max(maxNum, arr[i]);
    }
    return maxNum;
}

double d_max_in_arr(const double* arr, const int arr_size) {
    double maxNum = arr[0];
    for (int i=0; i<arr_size; i++){
        maxNum = d_max(maxNum, arr[i]);
    }
    return maxNum;
}
