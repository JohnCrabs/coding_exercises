# Read a given from the user N numbers and calculate the mean and standard deviation.
import math

def readNumerical(msg):
    uInput = ""
    while not uInput.replace('.', '', 1).replace('-', '', 1).isnumeric():
        print(msg)
        uInput = input()
    return float(uInput)

def askForNumber_N():
    num = int(readNumerical("Give the size of the list:"))
    if num < 1:
        num = 1
    return num

def readNumbers(value):
    X_arr = []
    for __i__ in range(value):
        X_arr.append(readNumerical(f"{__i__}) Give a value: "))
    return X_arr

def findMean(X_arr):
    size = X_arr.__len__()
    u_Sum = 0
    for __index__ in range(size):
        u_Sum += X_arr[ __index__ ]
    return round(u_Sum / size, 3)

def findStdev(X_arr):
    size = X_arr.__len__()
    u_Mean =findMean(X_arr)
    u_Stdev = 0
    for __index__ in range(size):
        u_Stdev += (X_arr[ __index__ ] - u_Mean) * (X_arr[ __index__ ] - u_Mean)
    return round(math.sqrt(u_Stdev / (size - 1)), 3)

if __name__ == "__main__":
    X = readNumbers(askForNumber_N())
    uMean = findMean(X)
    uStdev = findStdev(X)
    print(f"The mean is {uMean} and it's standard deviation is {uStdev}!")
