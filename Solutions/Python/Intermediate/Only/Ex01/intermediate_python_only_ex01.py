# Read an image file and:
#   * show the original image to screen.
#   * split the image to the original channels.
#   * create an image by changing the order of the channels (e.g., print the BGR).
#   * change its brightness.
#   * change its contrast values.
#   * calculate the negative image.
#   * apply a filter to the original image.
#   * save the edited images to file.
import cv2
import numpy as np

IMAGE_PATH = "SingleTree.jpeg"
# FILTER = np.array([[1, 0, -1], [2, 0, -2], [1, 0, -1]])
# FILTER = np.array([[2, -1, 0], [-1, 0, -1], [0, -1, 2]])
FILTER = np.array([[-1, -2, -1], [0, 0, 0], [1, 2, 1]])


def showImg(winTitle, img: np.ndarray):
    cv2.imshow(winTitle, img)
    cv2.waitKey()

def reconstructImg(b_red: np.ndarray, b_green: np.ndarray, b_blue: np.ndarray):
    rgb = np.array([b_blue.T, b_green.T, b_red.T], dtype="uint8").T
    return rgb

def readAndShowImage(img_path, imShow=False):
    img = cv2.imread(img_path)  # The image is opened as BGR
    if imShow:
        showImg(img_path, img)
    return img

def splitImageToBands_RGB(img: np.ndarray):
    return img.T[2].T, img.T[1].T, img.T[0].T

def changeBrightness(img: np.ndarray, alpha=1.0, beta=0.0):
    # alpha * img + beta
    img_tmp = img.copy()
    red, green, blue = splitImageToBands_RGB(img_tmp)
    red = np.array(red, dtype='int')
    green = np.array(green, dtype='int')
    blue = np.array(blue, dtype='int')

    np.clip(alpha * red + beta, 0, 255, out=red)
    np.clip(alpha * green + beta, 0, 255, out=green)
    np.clip(alpha * blue + beta, 0, 255, out=blue)

    img_tmp = reconstructImg(red, green, blue)

    return img_tmp

def changeContrast(img: np.ndarray, alpha=1.0):
    # alpha * ( img - min_val ) / (max_val - min_val) * img

    img_tmp = img.copy()
    red, green, blue = splitImageToBands_RGB(img_tmp)
    red = np.array(red, dtype='int')
    green = np.array(green, dtype='int')
    blue = np.array(blue, dtype='int')

    np.clip(alpha * ((red - red.min()) / (red.max() - red.min())) * red, 0, 255, out=red)
    np.clip(alpha * ((green - green.min()) / (green.max() - green.min())) * green, 0, 255, out=green)
    np.clip(alpha * ((blue - blue.min()) / (blue.max() - blue.min())) * blue, 0, 255, out=blue)

    img_tmp = reconstructImg(red, green, blue)

    return img_tmp

def getNegativeImg(img: np.ndarray):
    # 255 - img
    img_tmp = 255 - img.copy()
    return img_tmp

def imageFilteringRGB(img: np.ndarray, f: np.ndarray):
    img_tmp = img.copy()
    red, green, blue = splitImageToBands_RGB(img_tmp)

    red = imageFilteringMONO(red, f)
    green = imageFilteringMONO(green, f)
    blue = imageFilteringMONO(blue, f)

    rgb = reconstructImg(red, green, blue)
    return rgb

def imageFilteringMONO(img: np.ndarray, f: np.ndarray):
   f_img = cv2.filter2D(src=img.copy(), ddepth=-1, kernel=f)
   return f_img

def exportImg(path: str, img: np.ndarray):
    cv2.imwrite(path, img)

if __name__ == "__main__":
    uImg = readAndShowImage(IMAGE_PATH, True)
    imgR, imgG, imgB = splitImageToBands_RGB(uImg)
    imgBrightness = changeBrightness(uImg, 1.0, 50.0)
    imgContrast = changeContrast(uImg, 1.5)
    imgNegative = getNegativeImg(uImg)
    imgFiltered  = imageFilteringRGB(uImg, FILTER)
    imgForExport = changeContrast(imgFiltered + uImg, 1.5)
    exportImg("editedImg.png", imgForExport)
