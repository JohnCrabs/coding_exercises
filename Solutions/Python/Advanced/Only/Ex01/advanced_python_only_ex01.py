"""
1) read a WAV soundtrack and:
   1) perform a statistical analysis (min, max, mean, stdev, median)
   2) run a 1D median filter (i.e., [1/3, 1/3, 1/3])
   3) using sklearn train a linear regressor to predict the next value of the soundtrack given as input the previous 5 values
   4) by using part of the soundtrack (i.e. the first 20 values) try to predict the next 100 values creating a new soundtrack.
   5) export the new soundtrack to wav file.
"""

import wave
import numpy as np
import sounddevice as sd

import scipy.io.wavfile as wavfile

from sklearn import preprocessing, svm
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt

from PIL import Image

KEY_MAX = "max"
KEY_MIN = "min"
KEY_MEAN = "mean"
KEY_STDEV = "stdev"
KEY_MEDIAN = "median"

class Soundtrack:
    def __init__(self):
        self.soundtrack = None
        self.audio: np.ndarray = None
        self.statistics = {}
        self.f_soundtrack = None
        self.model = None
        self.input_size = 0
        self.p_soundtrack = None


    def open_wav(self, path):
        self.soundtrack = wave.open(path, "rb")
        self.create_audio()
        self.calculate_statistics()

    def create_audio(self):
        samples = self.soundtrack.getnframes()
        audio = self.soundtrack.readframes(samples)
        # Convert buffer to float32 using NumPy
        audio_as_np_int16 = np.frombuffer(audio, dtype=np.int16)
        self.audio = audio_as_np_int16.astype(np.float32)
        valMax = self.audio.max()
        if valMax == 0:
            valMax = 1
        self.audio = self.audio / valMax

    def calculate_statistics(self):
        self.statistics[KEY_MAX] = self.audio.max()
        self.statistics[KEY_MIN] = self.audio.min()
        self.statistics[KEY_MEAN] = self.audio.mean()
        self.statistics[KEY_STDEV] = self.audio.std()
        self.statistics[KEY_MEDIAN] = np.nanmedian(self.audio)

    def filter_soundtrack(self, f):
        self.f_soundtrack = np.convolve(self.audio, f)

    def train_model(self, inputSize=5):
        X = []
        y = []
        self.input_size = inputSize
        audio = self.audio
        for __index__ in range(self.audio.shape[0]-inputSize):
            X.append(audio[__index__:__index__+inputSize:1])
            y.append(audio[__index__+inputSize])
        X_train, X_test, y_train, y_test = train_test_split(X, y,test_size=0.25)
        self.model = LinearRegression()
        self.model.fit(X_train, y_train)
        print(f"Acc = {self.model.score(X_test, y_test).__round__(5)}")

    def pred_soundtrack(self, start_audio, iters):
        start_audio = np.array(start_audio)
        if start_audio.shape[0] < self.input_size:
            print(f"Error::start_audio needs to be at least of size {self.input_size}")
            return

        p_audio = np.array(start_audio/start_audio.max()).tolist()
        for __index__ in range(iters):
            print(f"Predict {__index__} out of {iters-1} iterations...")
            X = np.array(p_audio, dtype="float32")[-self.input_size:]
            p_audio.append(self.model.predict(X.reshape(1, -1))[0])

        self.p_soundtrack = np.array(p_audio)

    def image_to_soundtrack(self, img_path: str, mode:int=0):
        img = np.array(Image.open(img_path).convert("L").getdata(), dtype="float32")
        rand_id = np.array(list(range(0, img.shape[0])))
        np.random.shuffle(rand_id)
        maxVal = img.max()
        if maxVal == 0:
            maxVal = 1
        img = img / maxVal
        img = np.array(2 * (img - img.min()) / (maxVal - img.min()) - 1, dtype="float32")[rand_id]
        img = np.convolve(img, [-1/2, 0, 1/2])

        X = []
        if mode == 0:
            for __index__ in range(img.shape[0] - self.input_size):
                X.append(img[__index__:__index__+self.input_size:1])
        else:
            for __index__ in range(int(img.shape[0] / self.input_size) - 1):
                X.append(img[__index__*self.input_size:__index__*self.input_size+self.input_size:1])

        self.p_soundtrack = self.model.predict(X)

    @staticmethod
    def show_soundtrack(in_soundtrack):
        plt.plot(in_soundtrack)
        plt.show()

    def play_original(self):
        sd.play(self.audio / self.audio.max())

    def play_filtered(self):
        sd.play(self.f_soundtrack / self.f_soundtrack.max())

    def play_predicted(self, ratio=44100):
        sd.play(self.p_soundtrack / self.p_soundtrack.max(), ratio)

    @staticmethod
    def write_wav(path:str, o_sound:np.ndarray, rate:int=44100):
        wavfile.write(path, rate, o_sound)

if __name__ == "__main__":
    wav_path = "sound_2.wav"
    img_path = "img.jpeg"
    sound = Soundtrack()
    sound.open_wav(wav_path)
    sound.filter_soundtrack([2, 1, 0, -1, -2])
    sound.train_model(inputSize=5)
    # sound.pred_soundtrack([0.32, 0.48, 0.55, 0.28, 0.35], 50000)
    sound.image_to_soundtrack(img_path=img_path, mode=0)
    sound.write_wav("./from_img.wav", sound.p_soundtrack)