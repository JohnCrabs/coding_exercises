# read a given from the user N numbers and return the maximum, minimum, mean and standard deviation.
import math

def readNumerical(msg):
    uInput = ""
    while not uInput.replace('.', '', 1).replace('-', '', 1).isnumeric():
        print(msg)
        uInput = input()
    return float(uInput)

def askForNumber_N():
    num = int(readNumerical("Give the size of the list:"))
    if num < 1:
        num = 1
    return num

def readNumbers(value):
    X_arr = []
    for __i__ in range(value):
        X_arr.append(readNumerical(f"{__i__}) Give a value: "))
    return X_arr

def userMax(num_1, num_2):
    u_Max = num_1
    if num_2 > u_Max:
        u_Max = num_2
    return u_Max

def findMax(X_arr):
    size = X_arr.__len__()
    u_Max = X_arr[0]
    for __index__ in range(size):
        u_Max = userMax(u_Max, X_arr[__index__])
    return u_Max

def userMin(num_1, num_2):
    u_Min = num_1
    if num_2 < u_Min:
        u_Min = num_2
    return u_Min

def findMin(X_arr):
    size = X_arr.__len__()
    u_Min = X_arr[0]
    for __index__ in range(size):
        u_Min = userMin(u_Min, X_arr[__index__])
    return u_Min

def findMean(X_arr):
    size = X_arr.__len__()
    u_Sum = 0
    for __index__ in range(size):
        u_Sum += X_arr[ __index__ ]
    return round(u_Sum / size, 3)

def findStdev(X_arr):
    size = X_arr.__len__()
    u_Mean =findMean(X_arr)
    u_Stdev = 0
    for __index__ in range(size):
        u_Stdev += (X_arr[ __index__ ] - u_Mean) * (X_arr[ __index__ ] - u_Mean)
    return round(math.sqrt(u_Stdev / (size - 1)), 3)

if __name__ == "__main__":
    X = readNumbers(askForNumber_N())
    uMax = findMax(X)
    uMin = findMin(X)
    uMean = findMean(X)
    uStdev = findStdev(X)
    print(f"Maximum: {uMax}\nMinimum: {uMin}\nMean: {uMean}\nStdev: {uStdev}")