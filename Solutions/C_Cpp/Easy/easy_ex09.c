/* read a given from the user N numbers and sort them in DECREMENT order. */

#include <stdio.h>
#include <stdlib.h>

void getLine(char* line, const int line_size);

int i_readNumber();
float f_readNumber();
double d_readNumber();

void i_sort_decrement(int* arr, const int arr_size);
void f_sort_decrement(float* arr, const int arr_size);
void d_sort_decrement(double* arr, const int arr_size);

void exec_program();

int main() {
    exec_program();
    return 0;
}

void exec_program(){
    /* Ask the user for the size of the array. */
    printf("Give the size of the list: ");
    const int arr_size = i_readNumber();
    printf("Number size: %d\n", arr_size);

    /* Allocate memory */
    float* num_arr;
    num_arr = (float*)malloc(arr_size*sizeof(float));

    /* Ask the user to specify the numbers. */
    for(int i=0; i<arr_size; i++){
        printf("Give number #%d: ", i+1);
        num_arr[i] = f_readNumber();
    }

    /* Show him the numbers he selected. */
    printf("\nOriginal order: ");
    for(int i=0; i<arr_size-1; i++)
        printf("%f, ", num_arr[i]);
    printf("%f\n", num_arr[arr_size-1]);

    /* Sort. */
    f_sort_decrement(num_arr, arr_size);
    printf("Decrement order: ");
    for(int i=0; i<arr_size-1; i++)
        printf("%f, ", num_arr[i]);
    printf("%f\n", num_arr[arr_size-1]);

    /* Free allocated memory */
    free(num_arr);
}

void getLine(char* line, const int line_size) {
    int index = 0;
    char c;
    while(index < line_size && (c=getchar())!=EOF && ((c >='0' && c <='9') || c == '.' || (c=='-' && index==0) || (c=='+' && index==0))){
        line[index++] = c;
    }
    if (index >= line_size){
        while((c=getchar())!='\n') ;
    }
}

int i_readNumber() {
    const int line_size = 9;
    char* line = (char *)malloc(line_size * sizeof(char));
    getLine(line, line_size);
    int number = atoi(line);
    free(line);
    return number;
}

float f_readNumber() {
    const int line_size = 9;
    char* line = malloc(line_size*sizeof(char));
    getLine(line, line_size);
    float number = atof(line);
    free(line);
    return number;
}

double d_readNumber() {
    const int line_size = 9;
    char* line = malloc(line_size*sizeof(char));
    getLine(line, line_size);
    double number = atof(line);
    free(line);
    return number;
}

void i_sort_decrement(int* arr, const int arr_size){
    /* Parse the array elements */
    for(int i=0; i < arr_size-1; i++){
        /* If current index is higher then the next */
        if(arr[i] < arr[i+1]){
            int tmp_val = arr[i]; /* store the value in temporary variable */
            /* change the values */
            arr[i] = arr[i+1];
            arr[i+1] = tmp_val;
            /* reset the index: set it to -1
            so it can be increased by the for to 0 */
            i=-1;
        }
    }

}

void f_sort_decrement(float* arr, const int arr_size){
    /* Parse the array elements */
    for(int i=0; i < arr_size-1; i++){
        /* If current index is higher then the next */
        if(arr[i] < arr[i+1]){
            float tmp_val = arr[i]; /* store the value in temporary variable */
            /* change the values */
            arr[i] = arr[i+1];
            arr[i+1] = tmp_val;
            /* reset the index: set it to -1
            so it can be increased by the for to 0 */
            i=-1;
        }
    }
}

void d_sort_decrement(double* arr, const int arr_size){
    /* Parse the array elements */
    for(int i=0; i < arr_size-1; i++){
        /* If current index is higher then the next */
        if(arr[i] < arr[i+1]){
            double tmp_val = arr[i]; /* store the value in temporary variable */
            /* change the values */
            arr[i] = arr[i+1];
            arr[i+1] = tmp_val;
            /* reset the index: set it to -1
            so it can be increased by the for to 0 */
            i=-1;
        }
    }
}

