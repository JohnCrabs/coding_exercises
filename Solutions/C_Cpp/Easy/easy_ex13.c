/* Read a given from the user N numbers and return the least integer dividers. */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void getLine(char* line, const int line_size);
int i_readNumber();
int i_min(int num1, int num2);
int i_min_in_arr(const int* arr, const int arr_size);
int i_getDividers(int** arr, const int number);
int i_getCommonDividers(int** dst, const int** div_arrs, const int* arr_sizes, const int arr_size);

void exec_program();

int main(){
    exec_program();

    return 0;
}

void exec_program(){
    /* Ask the user for the size of the number array. */
    printf("Give the size of the list: ");
    const int arr_size = i_readNumber();
    printf("Number size: %d\n", arr_size);

    /* Allocate memory for number array */
    int* num_arr;
    num_arr = (int*)malloc(arr_size*sizeof(int));

    /* Ask the user to specify the numbers. */
    for(int i=0; i<arr_size; i++){
        printf("Give number #%d: ", i+1);
        num_arr[i] = i_readNumber();
    }

    /* Allocate memory for the dividers */
    int** div_arr = (int**)malloc(arr_size*sizeof(int*));
    int* div_arr_size = (int*)malloc(arr_size*sizeof(int));


    /* For each number find its dividers */
    for(int i=0; i<arr_size; i++){
        div_arr[i] = NULL;
        div_arr_size[i] = i_getDividers(&div_arr[i], num_arr[i]);
    }

    /* Print the dividers list */
    for(int i=0; i<arr_size; i++){
     printf("\nDividers for Number %d (%d): ", num_arr[i], div_arr_size[i]);
        for(int j=0; j<div_arr_size[i]; j++){
            printf("%d ", div_arr[i][j]);
        }
    }
    printf("\n");

    /* Allocate memory and find the common dividers */
    int* commonDividers = NULL;
    int common_div_size = i_getCommonDividers(&commonDividers, (const int**)div_arr, div_arr_size, arr_size);

    /* Print dividers */
    printf("\nCommon Dividers (%d): ", common_div_size);
    for(int i=0; i<common_div_size; i++){
        printf("%d ", commonDividers[i]);
    }
    printf("\n");

    if(common_div_size > 1)
        printf("\nLeast Common Divider: %d", commonDividers[1]);
    else
        printf("\nLeast Common Divider: 1");
    printf("\n");

    /* Free allocated memory */
    free(num_arr);
    for(int i=0; i<arr_size; i++){
        free(div_arr[i]);
    }
    free(div_arr_size);
    free(commonDividers);

}

void getLine(char* line, const int line_size) {
    int index = 0;
    char c;
    while(index < line_size && (c=getchar())!=EOF && ((c >='0' && c <='9') || c == '.' || (c=='-' && index==0) || (c=='+' && index==0))){
        line[index++] = c;
    }
    if (index >= line_size){
        while((c=getchar())!='\n') ;
    }
}

int i_readNumber() {
    const int line_size = 9;
    char* line = (char *)malloc(line_size * sizeof(char));
    getLine(line, line_size);
    int number = atoi(line);
    free(line);
    return number;
}

int i_min(int num1, int num2) {
    return num1 < num2 ? num1 : num2;
}

int i_min_in_arr(const int* arr, const int arr_size) {
    int minNum = arr[0];
    for (int i=0; i<arr_size; i++){
        minNum = i_min(minNum, arr[i]);
    }
    return minNum;
}

int i_getDividers(int** arr, const int number) {
    int arr_size = 1;
    for(int i=1; i<=number; i++){
        if((number%i) == 0){
            (*arr) = realloc((*arr), arr_size*sizeof(int));
            (*arr)[arr_size-1] = i;
            arr_size++;
        }
    }
    return arr_size - 1;
}

int i_getCommonDividers(int** dst, const int** div_arrs, const int* arr_sizes, const int arr_size){
    int min_size = i_min_in_arr(arr_sizes, arr_size);
    (*dst) = NULL;

    int min_index = 0;
    for(int i=0; i<arr_size; i++){
        if(min_size == arr_sizes[i]){
            min_index = i;
            break;
        }
    }

    int tmp_index = 1;
    for(int i=0; i<min_size; i++){
        int canBeAdded_index = 1;
        for(int j=0; j<arr_size; j++){
            if(j != min_index){
                for(int k=0; k<arr_sizes[j]; k++){
                    if(div_arrs[min_index][i] == div_arrs[j][k]){
                        canBeAdded_index++;
                        break;
                    }
                }
            }
        }
        if(canBeAdded_index == arr_size){
            (*dst) = realloc((*dst), tmp_index*sizeof(int));
            (*dst)[tmp_index-1] = div_arrs[min_index][i];
            tmp_index++;
        }
    }

    tmp_index--;
    return tmp_index;
}
