/* Read a given from the user N numbers and return the MEDIAN number. */
#include <stdio.h>
#include <string.h>  /* for memcpy */
#include <stdlib.h>

void getLine(char* line, const int line_size);

int i_readNumber();
float f_readNumber();
double d_readNumber();

void i_sort_increment(int* arr, const int arr_size);
void f_sort_increment(float* arr, const int arr_size);
void d_sort_increment(double* arr, const int arr_size);

int i_median_in_arr(const int* arr, const int arr_size);
float f_median_in_arr(const float* arr, const int arr_size);
double d_median_in_arr(const double* arr, const int arr_size);

void exec_program();

int main() {
    exec_program();
    return 0;
}

void exec_program(){
    /* Ask the user for the size of the array. */
    printf("Give the size of the list: ");
    const int arr_size = i_readNumber();
    printf("Number size: %d\n", arr_size);

    /* Allocate memory */
    float* num_arr;
    num_arr = (float*)malloc(arr_size*sizeof(float));

    /* Ask the user to specify the numbers. */
    for(int i=0; i<arr_size; i++){
        printf("Give number #%d: ", i+1);
        num_arr[i] = f_readNumber();
    }

    /* Show him the numbers he selected. */
    printf("\nOriginal order: ");
    for(int i=0; i<arr_size-1; i++)
        printf("%f, ", num_arr[i]);
    printf("%f\n", num_arr[arr_size-1]);

    /* Median. */
    float medianVal = f_median_in_arr(num_arr, arr_size);
    printf("\nMedian = %f\n", medianVal);

    /* Free allocated memory */
    free(num_arr);
}

void getLine(char* line, const int line_size) {
    int index = 0;
    char c;
    while(index < line_size && (c=getchar())!=EOF && ((c >='0' && c <='9') || c == '.' || (c=='-' && index==0) || (c=='+' && index==0))){
        line[index++] = c;
    }
    if (index >= line_size){
        while((c=getchar())!='\n') ;
    }
}

int i_readNumber() {
    const int line_size = 9;
    char* line = (char *)malloc(line_size * sizeof(char));
    getLine(line, line_size);
    int number = atoi(line);
    free(line);
    return number;
}

float f_readNumber() {
    const int line_size = 9;
    char* line = malloc(line_size*sizeof(char));
    getLine(line, line_size);
    float number = atof(line);
    free(line);
    return number;
}

double d_readNumber() {
    const int line_size = 9;
    char* line = malloc(line_size*sizeof(char));
    getLine(line, line_size);
    double number = atof(line);
    free(line);
    return number;
}

void i_sort_increment(int* arr, const int arr_size){
    /* Parse the array elements */
    for(int i=0; i < arr_size-1; i++){
        /* If current index is higher then the next */
        if(arr[i] > arr[i+1]){
            int tmp_val = arr[i]; /* store the value in temporary variable */
            /* change the values */
            arr[i] = arr[i+1];
            arr[i+1] = tmp_val;
            /* reset the index: set it to -1
            so it can be increased by the for to 0 */
            i=-1;
        }
    }

}

void f_sort_increment(float* arr, const int arr_size){
    /* Parse the array elements */
    for(int i=0; i < arr_size-1; i++){
        /* If current index is higher then the next */
        if(arr[i] > arr[i+1]){
            float tmp_val = arr[i]; /* store the value in temporary variable */
            /* change the values */
            arr[i] = arr[i+1];
            arr[i+1] = tmp_val;
            /* reset the index: set it to -1
            so it can be increased by the for to 0 */
            i=-1;
        }
    }
}

void d_sort_increment(double* arr, const int arr_size){
    /* Parse the array elements */
    for(int i=0; i < arr_size-1; i++){
        /* If current index is higher then the next */
        if(arr[i] > arr[i+1]){
            double tmp_val = arr[i]; /* store the value in temporary variable */
            /* change the values */
            arr[i] = arr[i+1];
            arr[i+1] = tmp_val;
            /* reset the index: set it to -1
            so it can be increased by the for to 0 */
            i=-1;
        }
    }
}

int i_median_in_arr(const int* arr, const int arr_size) {
    int *arr_cpy = (int*)malloc(sizeof(int)*arr_size);
    memcpy(arr_cpy, arr, sizeof(int)*arr_size);
    i_sort_increment(arr_cpy, arr_size);
    int medianValue = arr_cpy[arr_size/2];
    free(arr_cpy);
    return medianValue;
}

float f_median_in_arr(const float* arr, const int arr_size) {
    float *arr_cpy = (float*)malloc(sizeof(float)*arr_size);
    memcpy(arr_cpy, arr, sizeof(float)*arr_size);
    f_sort_increment(arr_cpy, arr_size);
    float medianValue = arr_cpy[arr_size/2];
    free(arr_cpy);
    return medianValue;
}

double d_median_in_arr(const double* arr, const int arr_size) {
    double *arr_cpy = (double*)malloc(sizeof(double)*arr_size);
    memcpy(arr_cpy, arr, sizeof(double)*arr_size);
    d_sort_increment(arr_cpy, arr_size);
    double medianValue = arr_cpy[arr_size/2];
    free(arr_cpy);
    return medianValue;
}

