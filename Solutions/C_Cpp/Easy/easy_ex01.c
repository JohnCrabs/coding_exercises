/* Read two numbers and return the MAXIMUM number */
#include <stdio.h>
#include <stdlib.h>

void getLine(char* line, const int line_size);

int i_readNumber();
float f_readNumber();
double d_readNumber();

int i_max(int num1, int num2);
float f_max(float num1, float num2);
double d_max(double num1, double num2);

int main(){
    printf("Give first number: ");
    float num1 = f_readNumber();

    printf("Give second number: ");
    float num2 = f_readNumber();

    float maxNumber = f_max(num1, num2);
    printf("Max number: %f", maxNumber);


    return 0;
}

void getLine(char* line, const int line_size){
    int index = 0;
    char c;
    while(index < line_size && (c=getchar())!=EOF && ((c >='0' && c <='9') || c == '.' || (c=='-' && index==0) || (c=='+' && index==0))){
        line[index++] = c;
    }
    if (index >= line_size){
        while((c=getchar())!='\n') ;
    }
}


int i_readNumber(){
    const int line_size = 9;
    char* line = (char *)malloc(line_size * sizeof(char));
    getLine(line, line_size);
    int number = atoi(line);
    free(line);
    return atoi(line);
}

float f_readNumber(){
    const int line_size = 9;
    char* line = malloc(line_size*sizeof(char));
    getLine(line, line_size);
    float number = atof(line);
    free(line);
    return number;
}

double d_readNumber(){
    const int line_size = 9;
    char* line = malloc(line_size*sizeof(char));
    getLine(line, line_size);
    double number = atof(line);
    free(line);
    return number;
}

int i_max(int num1, int num2){
    return num1 > num2 ? num1 : num2;
}

float f_max(float num1, float num2){
    return num1 > num2 ? num1 : num2;
}

double d_max(double num1, double num2){
    return num1 > num2 ? num1 : num2;
}
