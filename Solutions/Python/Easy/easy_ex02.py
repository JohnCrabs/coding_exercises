# Read two numbers and return the MINIMUM number

def readNumbers(value):
    X_arr = []
    for __i__ in range(value):
        uInput = ""
        while not uInput.replace('.', '', 1).replace('-', '', 1).isnumeric():
            print(f"{__i__}) Give a value: ")
            uInput = input()
            X_arr.append(float(uInput))
    return X_arr

def userMin(num_1, num_2):
    u_Min = num_1
    if num_2 < u_Min:
        u_Min = num_2
    return u_Min

if __name__ == "__main__":
    X = readNumbers(2)
    uMin = userMin(X[0], X[1])
    print(f"Number {uMin} is the minimum number!")
