/* Read a given from the user N numbers and calculate the MEAN and STANDARD DEVIATION. */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void getLine(char* line, const int line_size);

int i_readNumber();
float f_readNumber();
double d_readNumber();

int i_mean_in_arr(const int* arr, const int arr_size);
float f_mean_in_arr(const float* arr, const int arr_size);
double d_mean_in_arr(const double* arr, const int arr_size);

int i_stdev_in_arr(const int* arr, const int arr_size);
float f_stdev_in_arr(const float* arr, const int arr_size);
double d_stdev_in_arr(const double* arr, const int arr_size);

void exec_program();

int main() {
    exec_program();
    return 0;
}

void exec_program(){
    /* Ask the user for the size of the array. */
    printf("Give the size of the list: ");
    const int arr_size = i_readNumber();
    printf("Number size: %d\n", arr_size);

    /* Allocate memory */
    float* num_arr;
    num_arr = (float*)malloc(arr_size*sizeof(float));

    /* Ask the user to specify the numbers. */
    for(int i=0; i<arr_size; i++){
        printf("Give number #%d: ", i+1);
        num_arr[i] = f_readNumber();
    }

    /* Show the numbers he selected. */
    for(int i=0; i<arr_size-1; i++)
        printf("%f, ", num_arr[i]);
    printf("%f\n", num_arr[arr_size-1]);

    /* Find the mean and stdev number and print it to screen. */
    float meanNum = f_mean_in_arr(num_arr, arr_size);
    float stdev = f_stdev_in_arr(num_arr, arr_size);
    printf("Mean: % f\n", meanNum);
    printf("Stdev: %f\n", stdev);

    /* Free allocated memory */
    free(num_arr);
}

void getLine(char* line, const int line_size) {
    int index = 0;
    char c;
    while(index < line_size && (c=getchar())!=EOF && ((c >='0' && c <='9') || c == '.' || (c=='-' && index==0) || (c=='+' && index==0))){
        line[index++] = c;
    }
    if (index >= line_size){
        while((c=getchar())!='\n') ;
    }
}

int i_readNumber() {
    const int line_size = 9;
    char* line = (char *)malloc(line_size * sizeof(char));
    getLine(line, line_size);
    int number = atoi(line);
    free(line);
    return number;
}

float f_readNumber() {
    const int line_size = 9;
    char* line = malloc(line_size*sizeof(char));
    getLine(line, line_size);
    float number = atof(line);
    free(line);
    return number;
}

double d_readNumber() {
    const int line_size = 9;
    char* line = malloc(line_size*sizeof(char));
    getLine(line, line_size);
    double number = atof(line);
    free(line);
    return number;
}

int i_mean_in_arr(const int* arr, const int arr_size) {
    int mean = 0;
    for (int i=0; i<arr_size; i++){
        mean += arr[i];
    }

    mean /= arr_size;
    return mean;
}

float f_mean_in_arr(const float* arr, const int arr_size) {
    float mean = 0;
    for (int i=0; i<arr_size; i++){
        mean += arr[i];
    }
    mean /= arr_size;
    return mean;
}

double d_mean_in_arr(const double* arr, const int arr_size) {
    double mean = 0;
    for (int i=0; i<arr_size; i++){
        mean += arr[i];
    }
    mean /= arr_size;
    return mean;
}

int i_stdev_in_arr(const int* arr, const int arr_size) {
    int mean = i_mean_in_arr(arr, arr_size);
    int stdev = 0;
    for (int i=0; i<arr_size; i++){
        stdev += (mean - arr[i]) * (mean - arr[i]);
    }
    stdev /= (arr_size - 1);
    stdev = sqrt(stdev);
    return stdev;
}

float f_stdev_in_arr(const float* arr, const int arr_size) {
    float mean = f_mean_in_arr(arr, arr_size);
    float stdev = 0;
    for (int i=0; i<arr_size; i++){
        stdev += (mean - arr[i]) * (mean - arr[i]);
    }
    stdev /= (arr_size - 1);
    stdev = sqrt(stdev);
    return stdev;
}

double d_stdev_in_arr(const double* arr, const int arr_size) {
    double mean = d_mean_in_arr(arr, arr_size);
    double stdev = 0;
    for (int i=0; i<arr_size; i++){
        stdev += (mean - arr[i]) * (mean - arr[i]);
    }
    stdev /= (arr_size - 1);
    stdev = sqrt(stdev);
    return stdev;
}
