/* Read a file and print it to the screen. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int i_read_file(const char* path, char** dst);

void exec_program();

int main(){
    exec_program();
    return 0;
}

void exec_program(){
    char* file_path = "./coordinates.txt";
    char* file_text = NULL;
    int file_size = i_read_file(file_path, &file_text);

    printf("---------------------");
    printf("\nFILE CONTEXT:\n");
    printf("---------------------");
    printf("\n%s\n", file_text);
    printf("---------------------");
}

int i_read_file(const char* path, char** dst){
    FILE* fp;
    fp = fopen(path, "r");

    if (fp == NULL){
        printf("Error::File cannot open. Check the path::%s", path);
        return 0;
    }

    int file_size = 0;
    char c = '\0';
    while((c=fgetc(fp))!=EOF){
        (*dst) = realloc((*dst), (++file_size)*sizeof(char));
        (*dst)[file_size-1] = c;
    }

    return file_size;

}
