# Read two numbers and return the MAXIMUM number

def readNumbers(value):
    X_arr = []
    for __i__ in range(value):
        uInput = ""
        while not uInput.replace('.', '', 1).replace('-', '', 1).isnumeric():
            print(f"{__i__}) Give a value: ")
            uInput = input()
            X_arr.append(float(uInput))
    return X_arr

def userMax(num_1, num_2):
    u_Max = num_1
    if num_2 > u_Max:
        u_Max = num_2
    return u_Max

if __name__ == "__main__":
    X = readNumbers(2)
    uMax = userMax(X[0], X[1])
    print(f"Number {uMax} is the maximum number!")
