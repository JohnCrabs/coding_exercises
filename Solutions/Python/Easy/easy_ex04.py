# Read a given from the user N numbers and return the minimum number.

def readNumerical(msg):
    uInput = ""
    while not uInput.replace('.', '', 1).replace('-', '', 1).isnumeric():
        print(msg)
        uInput = input()
    return float(uInput)

def askForNumber_N():
    num = int(readNumerical("Give the size of the list:"))
    if num < 1:
        num = 1
    return num

def readNumbers(value):
    X_arr = []
    for __i__ in range(value):
        X_arr.append(readNumerical(f"{__i__}) Give a value: "))
    return X_arr

def userMin(num_1, num_2):
    u_Min = num_1
    if num_2 < u_Min:
        u_Min = num_2
    return u_Min

def findMin(X_arr):
    size = X_arr.__len__()
    u_Min = X_arr[0]
    for __index__ in range(size):
        u_Min = userMin(u_Min, X_arr[__index__])
    return u_Min

if __name__ == "__main__":
    X = readNumbers(askForNumber_N())
    uMin = findMin(X)
    print(f"Number {uMin} is the minimum number!")
