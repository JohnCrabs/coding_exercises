/* Read an integer number and return a list containing the integer dividers. */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void getLine(char* line, const int line_size);

int i_readNumber();

int i_getDividers(int** arr, const int number);

void exec_program();

int main(){
    exec_program();

    return 0;
}

void exec_program(){
    /* Ask the user for the size of the array. */
    printf("Give a number: ");
    const int number = i_readNumber();

    int* arr = NULL;
    int arr_size = i_getDividers(&arr, number);;

    printf("\nDividers Number: %d\n", arr_size);
    printf("Dividers: ");
    for(int i=0; i<arr_size; i++){
        printf("%d ", arr[i]);
    }
}

void getLine(char* line, const int line_size) {
    int index = 0;
    char c;
    while(index < line_size && (c=getchar())!=EOF && ((c >='0' && c <='9') || c == '.' || (c=='-' && index==0) || (c=='+' && index==0))){
        line[index++] = c;
    }
    if (index >= line_size){
        while((c=getchar())!='\n') ;
    }
}

int i_readNumber() {
    const int line_size = 9;
    char* line = (char *)malloc(line_size * sizeof(char));
    getLine(line, line_size);
    int number = atoi(line);
    free(line);
    return number;
}

int i_getDividers(int** arr, const int number) {
    int arr_size = 1;
    for(int i=1; i<=number; i++){
        if((number%i) == 0){
            (*arr) = realloc((*arr), arr_size*sizeof(int));
            (*arr)[arr_size-1] = i;
            arr_size++;
        }
    }
    return arr_size - 1;
}



