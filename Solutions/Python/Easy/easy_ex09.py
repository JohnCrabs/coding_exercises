def readNumerical(msg):
    uInput = ""
    while not uInput.replace('.', '', 1).replace('-', '', 1).isnumeric():
        print(msg)
        uInput = input()
    return float(uInput)

def askForNumber_N():
    num = int(readNumerical("Give the size of the list:"))
    if num < 1:
        num = 1
    return num

def readNumbers(value):
    X_arr = []
    for __i__ in range(value):
        X_arr.append(readNumerical(f"{__i__}) Give a value: "))
    return X_arr

def userSortDecreasing(X_arr):
    size = X_arr.__len__()
    index = 0
    while index < size-1:
        if X_arr[index] < X_arr[index+1]:
            tmp = X_arr[index]
            X_arr[index] = X_arr[index+1]
            X_arr[index+1] = tmp
            index = -1
        index += 1
    return X_arr

if __name__ == "__main__":
    X = readNumbers(askForNumber_N())
    X = userSortDecreasing(X)
    print(f"Array in decreasing order: {X}")