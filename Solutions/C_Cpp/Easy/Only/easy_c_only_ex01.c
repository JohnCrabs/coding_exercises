
#include <stdio.h>
#include <stdlib.h>

void getLine(char* line, const int line_size);

int i_readNumber();
float f_readNumber();
double d_readNumber();

int main(){
    printf("Give integer number: ");
    int num1 = i_readNumber();

    printf("Give float number: ");
    float num2 = f_readNumber();

    printf("Give double number: ");
    float num3 = d_readNumber();

    printf("Integer: %d\n", num1);
    printf("Float: %f\n", num2);
    printf("Double: %f\n", num3);

    return 0;
}

void getLine(char* line, const int line_size){
    int index = 0;
    char c;
    while(index < line_size && (c=getchar())!=EOF && ((c >='0' && c <='9') || c == '.' || (c=='-' && index==0) || (c=='+' && index==0))){
        line[index++] = c;
    }
    if (index >= line_size){
        while((c=getchar())!='\n') ;
    }
}

int i_readNumber(){
    const int line_size = 9;
    char* line = (char *)malloc(line_size * sizeof(char));
    getLine(line, line_size);
    int number = atoi(line);
    free(line);
    return atoi(line);
}

float f_readNumber(){
    const int line_size = 9;
    char* line = malloc(line_size*sizeof(char));
    getLine(line, line_size);
    float number = atof(line);
    free(line);
    return number;
}

double d_readNumber(){
    const int line_size = 9;
    char* line = malloc(line_size*sizeof(char));
    getLine(line, line_size);
    double number = atof(line);
    free(line);
    return number;
}
