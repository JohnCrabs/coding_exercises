# Coding Exercises
This repository contains several exercises for developing coding skills.
Its primary usage is educational; however, the solutions of some exercises
can be applied in real case examples.
If you just started coding, I suggest you to try and solve the exercises 
rather than navigating the solutions. 
If you are experienced or feel comfortable coding, you can use this 
repository as you like.

The solutions of the exercises will be given in C/C++ and Python languages.
In the **Setup** section I will provide all the necessary information for
setting up your machine and starting coding, including the libraries and 
tools.

**Note that** this repository is still under development, 
for this reason some things may not
be working appropriately. Feel free to contact me if you observed any problem, 
or you have suggestions.

## Setup

Python 3.9
```
pip install numpy opencv-python
```

## Easy Exercises
Develop software which:
1) read two numbers and return the **maximum** number.
2) read two numbers and return the **minimum** number.
3) read a given from the user **N** numbers and return the **maximum** number.
4) read a given from the user **N** numbers and return the **minimum** number.
5) read a given from the user **N** numbers and return the **maximum** and **minimum** number.
6) read a given from the user **N** numbers and calculate the **mean** and **standard deviation**.
7) read a given from the user **N** numbers and return the **maximum**, **minimum**, **mean** and **standard deviation**.
8) read a given from the user **N** numbers and sort them in **increment** order.
9) read a given from the user **N** numbers and sort them in **decrement** order.
10) read a given from the user **N** numbers and return the **median** number.
11) read an integer number and return a list containing the integer dividers.
12) read a given from the user **N** numbers and return the common integer dividers.
13) read a given from the user **N** numbers and return the least integer dividers.

### C/C++ Only
1) Develop a function for reading a number, including decimal and negative numbers, using getchar.

### Python Only

## Intermediate Exercises
Develop software which:
1) read a file and print it to the screen.
2) read a file with distance measurements and perform statistical analysis (max, min, mean, stdev, median).
3) read a file with coordinates of two points and find the line (equation) that passed for them.
4) read a file with coordinates of two points and find the Euclidian distance.
5) read a file with coordinates of two points and find the Manhattan distance.
6) read a file with coordinates of a triangle and a file with coordinates of a point and try to find if the point is inside or outside the triangle.

## Advanced Exercises
Develop software using python classes which:
1) read a WAV soundtrack and:
   1) perform a statistical analysis (min, max, mean, stdev, median)
   2) run a 1D median filter (i.e., [1/3, 1/3, 1/3])
   3) using sklearn train a linear regressor to predict the next value of the soundtrack given as input the previous 5 values
   4) by using the half of the soundtrack predict the next half values creating a new soundtrack.
   5) export the new soundtrack to wav file.

### Python Only
1) Read an image file and:
   1) show the original image to screen.
   2) split the image to the original channels.
   3) create an image by changing the order of the channels (e.g., print the BGR).
   4) change its brightness.
   5) change its contrast values.
   6) calculate the negative image.
   7) apply a filter to the original image.
   8) save the edited images to file.

## Difficult Exercises
1) read a file with several coordinates and try to find the best line and perform a statistical analysis (metrics: max, min, mean, stdev, mean).

### Python Only

## License
This project is licensed under the GNU General Public License v3.0 or later.